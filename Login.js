
// URL of top page.

var webdriver = require('selenium-webdriver');
var flow = webdriver.promise.controlFlow();
var chai = require('chai');
var assert = require('chai').assert;
var expect = chai.expect;
chai.use(require('chai-as-promised'));
var request = require('request');
var cheerio = require('cheerio');

process.on('uncaughtException', function (err) {
    console.log('Caught exception: ' + err.stack);
});

somefunction ();

function somefunction (){
    var driver = new webdriver.Builder().
        withCapabilities(webdriver.Capabilities.chrome()).
        build();
    Login (driver, "alex.taylor@doctorbase.com", "secret", "drmarksu@gmail.com");
}



function Login (driver, sUser, sPass, sImpersonatedUser){

    var userxpath = '//*[@id="login_username"]';
    var passxpath = '//*[@id="db_login_form"]/div[2]/input';
    var loginbtnxpath = '//*[@id="login"]';
    var impersonatebtnxpath = '//*[@id="header"]/div[2]/ul[1]/li/a'
    var impersonatefieldxpath = '//*[@id="content"]/div[2]/div/div[2]/form/div/input';
    var impersonatesearchxpath = '//*[@id="content"]/div[2]/div/div[2]/form/div/span/button'

    var targeturl = "https://stage.doctorbase.com/admin";
    driver.get(targeturl);

    var loginfield = driver.findElement(webdriver.By.xpath(userxpath));
    var passfield = driver.findElement(webdriver.By.xpath(passxpath));
    var btnLogin = driver.findElement(webdriver.By.xpath(loginbtnxpath));


    loginfield.sendKeys(sUser);
    passfield.sendKeys(sPass);
    btnLogin.click();

    driver.wait(function () {
        return driver.isElementPresent(webdriver.By.xpath(impersonatebtnxpath));
    }, 10000);

    var btnImpersonate = driver.findElement(webdriver.By.xpath(impersonatebtnxpath));
    btnImpersonate.click();
    driver.wait(function () {
        return driver.isElementPresent(webdriver.By.xpath(impersonatefieldxpath));
    }, 10000);
    var fldImpersonate = driver.findElement(webdriver.By.xpath(impersonatefieldxpath));
    var btnsearch = driver.findElement(webdriver.By.xpath(impersonatesearchxpath));
    fldImpersonate.sendKeys(sImpersonatedUser);
    btnsearch.click();
}