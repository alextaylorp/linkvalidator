
// URL of top page.
var url = 'http://DoctorBase.com/';
var pathArray = url.split( '/' );
var protocol = pathArray[0];
var host = pathArray[2];
var baseurl = protocol + '//' + host;
// Include modules
var request = require('request');
var cheerio = require('cheerio');
var webdriver = require('selenium-webdriver');
var flow = webdriver.promise.controlFlow();
var chai = require('chai');
var assert = require('chai').assert;
var expect = chai.expect;
chai.use(require('chai-as-promised'));

process.on('uncaughtException', function (err) {
    console.log('Caught exception: ' + err.stack);
});


// Run test.
ValidateChildLinks(url);

function ValidateChildLinks(sUrl){
    // Initialize the link array with the parent page.
    arLinks=[
        {
            url: url,
            level:0,
            parent: null
        }
    ];
    var tel ="x";
    // Get Level I links.
    GetChildLinksForLevel(arLinks , 0, function(arLevel1Links) {
        // Get Level II links.
        GetChildLinksForLevel(arLevel1Links , 1, function(arLevel1and2Links) {
            // Test the higher level links first.
            arLevel1and2Links.reverse();
            var driver = new webdriver.Builder().
                withCapabilities(webdriver.Capabilities.chrome()).
                build();
            var urlno=1
            while(a=arLevel1and2Links.pop()) {
                urlno++;
                var targeturl = a.url;
                targeturl = targeturl.toLowerCase();
                var pathArray = targeturl.split("/");
                var protocol = pathArray[0];
                var host = pathArray[2];

                // Navigate to target page using web driver.
                driver.get(targeturl);
                driver.sleep(1000);
                if (typeof host != "undefined"){
                    tel = host.slice(14, 18);
                }
                if (host == "doctorbase.com"  && pathArray[3] != "login" && pathArray[3] != "claim" && tel != "tel:") {
                    expect(driver.getCurrentUrl()).to.eventually.contain(host);
                    if (pathArray[3] != "blog") {
                        // Click the "home" link.
                        driver.findElement(webdriver.By.xpath("//img[@class='logo img-responsive']")).click();
                        expect(driver.getCurrentUrl()).to.eventually.contain(host);
                        driver.sleep(1000);

                        //  ------- TO DO:  ---------------------------------  //
                        //  --                                             --  //
                        //  --    Check for JavaScript Errors.             --  //
                        //  --    Perform other kinds of validations.      --  //
                        //  --                                             --  //
                        //  -------------------------------------------------  //
                    }
                }
                // Log test details to console.
                console.log( urlno + " " + targeturl + "\n    Level:" + a.level + " Parent: " + a.parent);
            }
        });
    })
}

// Get child links for all parent links of a given level.
function GetChildLinksForLevel(arLinks, iLevel, fn){
    var requests=0;
    arLinks.forEach(function (link) {
        var url = link.url;
        requests++;
        request(link, function (err, resp, body) {
            var url = link.url;
            var pathArray = url.split("/");
            var rooturl = pathArray[2].toLowerCase();

            if (rooturl=="doctorbase.com") {
                // Use Cheerio DOM module
                $ = cheerio.load(body);
                var links = $('a'); // JQuery to get all hyperlinks.
                $(links).each(function (i, link) {
                    var thishref = $(link).attr('href')
                    if (typeof thishref != "undefined") {

                        // Turn relative href into complete URL.
                        if (thishref.charAt(0) == 'h') {
                            var thisUrl = thishref;
                        }
                        else {
                            var thisUrl = baseurl + $(link).attr('href');
                        }
                        request({
                            uri: thisUrl,
                            method: "GET",
                            timeout: 10000,
                            followRedirect: true,
                            maxRedirects: 10
                        }, function (thiserr, thisresp, thisbody) {
                            //    �Verify Status Code of 200!
                            if (thisresp != undefined) {
                                thisStat = thisresp.statusCode;
                                assert.equal(thisresp.statusCode, '200', url);
                            }
                        });
                        if (thishref.charAt(0) != '#') {
                            // Add the link if not already added.
                            checkAndAdd(thisUrl, iLevel + 1, arLinks, url, thishref);
                        }
                    }
                });
            }
            requests--;
            if (requests==0) fn(arLinks);
        });
    });
}

// Check for redundancy
function checkAndAdd(thisUrl, thisLevel, arLinks, url, thishref) {
    var found =  arLinks.some(function(el) {
        return el.url === thisUrl;
    });
    if (!found) {
        arLinks.push({ url: thisUrl, href: thishref, level: thisLevel, parent: url }); }
    return found;
}