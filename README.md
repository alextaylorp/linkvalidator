
LinkValidator.js extracts all links from a web page and verifies a response code of 200 for each.
It can also navigate to each page using ChromeDriver to monitor things like inappropriate redirects.
Sit back and relax while the script finds all the problems for you :)

--SETUP STEPS--

1. Install an IDE such as JetBrains WebStorm.  https://www.jetbrains.com/webstorm/download/
(You can also run the script via command line, but the IDE adds a lot of value, especially in the debugging dept.).

2. Install NodeJS: https://nodejs.org/download/ (latest!).  Among other things, this will give you the "node.js Command Prompt".
I suggest using this command prompt for the command lines in the steps below.

3. Install the latest versions of Selenium WebDriver, ChromeDriver, Request and Cheerio, using the node.js Command Prompt.

    Commands:

        npm install --save selenium-webdriver@2.44.0

        npm install --save chromedriver@2.12.0

        npm install -g request cheerio

        npm install --save chai@1.10.0 chai-as-promised@4.1.1

    (request/cheerio enable "scraping", chai enables nice assertions)

4. Configure Webstorm to enable node.js.
Go to File >> Settings and expand the Languages&Framworks section on the left hand menu.
Select "Node.js and NPM"
In the "Node Interpreter" field, browse to the location of nodejs -- e.g. C:\Program Files (x86)\nodejs\node.exe

5. Open LinkValidator.js

6. Right click anywhere in the file and select Run 'LinkValidator.js'.

--LINK VALIDATOR--

1. Create an array of link objects with properties url, level and parent:

[{
    url: "http://doctorbase.com/",
    level:0,
    parent: null
 },
{
    url: "http://doctorbase.com/privacy",
    level:1,
    parent: "http://doctorbase.com/"
 }]


2. Navigate to each link object using ChromeDriver.

3. Using Chai, assert that the url is expected.

4. If the parent of the link is "http://doctorbase.com/", click the home link in the upper left and verify navigation to home page
(except the login and blog pages, which doesn't have a home link -- maybe they should).

