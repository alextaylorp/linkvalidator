
// Include modules
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var chai = require('chai');
var assert = require('chai').assert;
var expect = chai.expect;
chai.use(require('chai-as-promised'));
var url = 'http://DoctorBase.com/';
var pathArray = url.split( '/' );

process.on('uncaughtException', function (err) {
    console.log('Caught exception: ' + err);
});

// Run test.
//ValidateChildLinks(url);
RunTests();

function RunTests (){
    var inputFile=process.argv[2];
    //var inputFile="config.json"
    var obj;
    fs.readFile(inputFile, 'utf8', function (err, data) {
        if (err) throw err;
        obj = JSON.parse(data);
        var arRoots = obj.rootCrawlsURLS;
        for (i = 0; i < arRoots.length; i++) {
            var RootURL=arRoots[i];
            console.log(RootURL)
            ValidateChildLinks(RootURL)
        }
    });
}

function ValidateChildLinks(sUrl){
    // Initialize the link array with the parent page.
    arLinks=[
        {
            url: sUrl,
            level:0,
            parent: null
        }
    ];
    var tel ="x";
    // Get Level I links.
    GetChildLinksForLevel(arLinks , 0, function(arLevel1Links) {
        // Get Level II links.
        GetChildLinksForLevel(arLevel1Links , 1, function(arLevel1and2Links) {
            // Test the higher level links first.
            arLevel1and2Links.reverse();
            var urlno=1;
            while(a=arLevel1and2Links.pop()) {
                urlno++;
                var targeturl = a.url;
                var parent = a.parent;
                TestLink (targeturl, parent);
                //console.log( urlno + " " + targeturl + "\n    Level:" + a.level + " Parent: " + a.parent);
            }
        });
    })
}
function TestLink (targeturl, parent){
    if (parent==null){ parent="http://DoctorBase.com"}
    parent = parent.toLowerCase();
    targeturl = targeturl.toLowerCase();
    request(
        {
            uri: targeturl,
            method: "GET",
            timeout: 10000,
            followRedirect: true,
            maxRedirects: 10
        }
        , function (thiserr, thisresp, thisbody) {
            //    �Verify Status Code of 200!
            if (thisresp != undefined) {
                thisStat = thisresp.statusCode;
                //console.log (thisStat + targeturl)
                var pathArray = targeturl.split("/");
                var protocol = pathArray[0];
                var host = pathArray[2];
                var ParentpathArray = parent.split("/");
                var Parenthost = ParentpathArray[2];
                if (host=="doctorbase.com" && Parenthost=="doctorbase.com") {
                    assert.equal(thisresp.statusCode, '200', targeturl  +" Parent: " + parent);
                }
            }
        });
}
// Get child links for all parent links of a given level.
function GetChildLinksForLevel(arLinks, iLevel, fn){
    var requests=0;
    arLinks.forEach(function (link) {
        var url = link.url;
        requests++;
        request(link, function (err, resp, body) {
            var url = link.url;
            var pathArray = url.split("/");
            var protocol = pathArray[0];
            var host = pathArray[2];
            var baseurl = protocol + '//' + host;
            $ = cheerio.load(body);
            var links = $('a'); // JQuery to get all hyperlinks.
            $(links).each(function (i, link) {
                var thishref = $(link).attr('href')
                if (typeof thishref != "undefined") {
                    // Turn relative href into complete URL.
                    if (thishref.charAt(0) == 'h') {
                        var thisUrl = thishref;
                    }
                    else {
                        var thisUrl = baseurl + $(link).attr('href');
                    }
                    if (thishref.charAt(0) != '#') {
                        // Add the link if not already added.
                        checkAndAdd(thisUrl, iLevel + 1, arLinks, url, thishref);
                    }
                }
            });
            requests--;
            if (requests==0) fn(arLinks);
        });
    });
}
// Check for redundancy
function checkAndAdd(thisUrl, thisLevel, arLinks, url, thishref) {
    var found =  arLinks.some(function(el) {
        return el.url === thisUrl;
    });
    if (!found) {
        arLinks.push({ url: thisUrl, href: thishref, level: thisLevel, parent: url });
    }
    return found;
}