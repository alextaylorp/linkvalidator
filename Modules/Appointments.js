var webdriver = require('selenium-webdriver');
var login = require("../Utilities/Login.js");

CreatePortalAppointment ();

function CreatePortalAppointment (){
    var apptTabId='appointments';
    var createApptXpath = '//*[@class="btn btn-primary pull-left hidden-xs"]';
    var patientXpath='//*[@placeholder="Start typing patient\'s name..."]';
    var datexpath = '//*[@placeholder="Date"]';
    var durationXpath = '//*[@name="duration"]';
    var btnSubmitApptXpath = '//*[@data-bind="click: submit"]';
    var Appointment = {
        Patient: "Test Testing",
        Date: "Apr 28, 2015 11:45 AM",
        Duration: " ",
        Provider: " ",
        Chair: "No OP",
        Reason: "",
        Reminder: true,
        Survey: true
    };
    var driver = new webdriver.Builder().
        withCapabilities(webdriver.Capabilities.chrome()).
        build();
    login.LoginAndImpersonate (driver, "alex.taylor@doctorbase.com", "horse", "drmarksu@gmail.com");
    driver.wait(function () {
        return driver.isElementPresent(webdriver.By.id(apptTabId));
    }, 10000);
    driver.sleep (2000);
    var tabAppointments = driver.findElement(webdriver.By.id(apptTabId));
    tabAppointments.click();
    driver.wait(function () {
        return driver.isElementPresent(webdriver.By.xpath(createApptXpath));
    }, 10000);
    var btnCreateAppt = driver.findElement(webdriver.By.xpath(createApptXpath));
    btnCreateAppt.click();
    driver.wait(function () {
        return driver.isElementPresent(webdriver.By.xpath(patientXpath));
    }, 10000);
    var fldPatient = driver.findElement(webdriver.By.xpath(patientXpath));
    var fldDate = driver.findElement(webdriver.By.xpath(datexpath));
    var fldDuration = driver.findElement(webdriver.By.xpath(durationXpath));
    var btnSubmitAppt = driver.findElement(webdriver.By.xpath(btnSubmitApptXpath));
    fldPatient.sendKeys(Appointment.Patient);
    fldPatient.sendKeys(webdriver.Key.TAB);
    fldDate.sendKeys(webdriver.Key.chord(webdriver.Key.CONTROL, "a"));
    fldDate.sendKeys(Appointment.Date);
    fldDate.sendKeys(webdriver.Key.TAB);
    fldDuration.sendKeys(webdriver.Key.ARROW_DOWN);
    fldDuration.sendKeys(webdriver.Key.TAB);
    btnSubmitAppt.click();
}